﻿using Battleships.Base;
using Battleships.Managers;
using Battleships.Model;
using Battleships.View;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.View
{
    class BoardView : IObserver
    {
        GroupBox oceanBox;
        IPlayer assignedPlayer; //this player shoots at ships on this board

        public BoardView(IPlayer player, GroupBox groupBoxOcean)
        {
            assignedPlayer = player;
            oceanBox = groupBoxOcean;
        }


        public void DataUpdate()
        {
            if (oceanBox.Controls.Count<= assignedPlayer.Ocean.Width* assignedPlayer.Ocean.Height)
            {
                GenerateCell(assignedPlayer.CurrentOceanField);
            }
        }

        public void GenerateCell(OceanField oceanField)
        {
            OceanCellControl oceanCellCtrl = new OceanCellControl();
            oceanCellCtrl.Location = Location(oceanField.Coordinates.X, oceanField.Coordinates.Y, oceanCellCtrl.Width, oceanCellCtrl.Height);
            oceanCellCtrl.OceanField = oceanField;
            oceanBox.Controls.Add(oceanCellCtrl);
            oceanCellCtrl.AssignedPlayer = assignedPlayer;
            oceanField.AddObserver(oceanCellCtrl);
            oceanCellCtrl.Click += new EventHandler((sender, e) => OceanCellCtrl_Click(sender, e, oceanField, assignedPlayer));
            oceanField.NotifyObservers();
        }

        

        Point Location(int x, int y, int  width, int height)
        {
            return (new Point(20 + x* (width + 2), 20 + y * (height + 2)));
        }

        private void OceanCellCtrl_Click(object sender, EventArgs e, OceanField oceanField, IPlayer player)
        {
            if (assignedPlayer == GameManager.Instance.CurrentPlayer || GameManager.Instance.CurrentPlayer == GameManager.Instance.PlayerAi)
            {
                return;
            }
            GameManager.Instance.CurrentPlayer.CurrentOceanField = oceanField;
            GameManager.Instance.CurrentPlayer.FireCommand.Execute();//todo no GameManager here
        }



    }
}

