﻿using Battleships.Base;
using Battleships.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.View
{
    class HudView:IObserver
    {
        GroupBox hudBox;
        IPlayer assignedPlayer;
        List<UiShip> uiShips = new List<UiShip>();
        public HudView(IPlayer player,GroupBox groupBoxHud)
        {
            assignedPlayer = player;
            hudBox = groupBoxHud;

        }

        public void ShowShipsUiList(IPlayer player)
        {
            for (int i = 0; i < player.Ships.Count; i++)
            {
                UiShip uiShip = new UiShip();
                uiShip.Ship = player.Ships[i];
                uiShip.Location = new Point(10,15+( i * 40));
                player.Ships[i].AddObserver(uiShip);
                hudBox.Controls.Add(uiShip);
                player.Ships[i].NotifyObservers();
                uiShips.Add(uiShip);
            }
        }

        public void DataUpdate()
        {
            hudBox.Text = assignedPlayer.Name;
            if (uiShips.Count<=assignedPlayer.Ships.Count)
            {
                ShowShipsUiList(assignedPlayer);
            }


        }
    }
}
