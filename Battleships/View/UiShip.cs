﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Battleships.Base;
using Battleships.Model;

namespace Battleships.View
{
    public partial class UiShip : UserControl, IObserver
    {
        TextBox TextBox1;
        public IShip Ship;

        public UiShip()
        {
            InitializeComponent();
        }

        public void DataUpdate()
        {
                 TextBox1.Text = String.Format("{0}: {1} parts, destroyed: {2}", Ship.Name, Ship.ShipParts.Count, Ship.Destroyed);
        }

        private void UiShip_Load(object sender, EventArgs e)
        {
            TextBox1 = textBox1;
        }
    }
}
