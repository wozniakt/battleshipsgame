﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Battleships.Model;
using Battleships.Base;
using Battleships.Managers;

namespace Battleships.View
{
    public partial class OceanCellControl : UserControl, IObserver
    {

        public OceanField OceanField;
        public IPlayer AssignedPlayer;

        public OceanCellControl()
        {
            InitializeComponent();
        }

        public void DataUpdate()
        {
            BackColor = AssignedPlayer.OceanColor;
            switch (OceanField.AlreadyHit)
            {
                case true:
                    BackColor = Color.Black;
                    return;
                default:
                    break;
            }

            if (OceanField.AssignedShipPart==null)
            {
                return;
            }
            switch (OceanField.AssignedShipPart.Destroyed)
            {
                case true:
    
                    BackColor = Color.Red;
                    break;
                case false:
                    if (AssignedPlayer == GameManager.Instance.PlayerAi)
                    {
                        BackColor = AssignedPlayer.OceanColor;
                    }
                    else
                    {
                        BackColor = AssignedPlayer.ShipsColor;
                    }

                    break;
                default:
                    break;
            }
        }
    }
}
