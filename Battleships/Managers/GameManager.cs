﻿using Battleships.Base;
using Battleships.Command;
using Battleships.Model;
using Battleships.View;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.Managers
{
    public class GameManager : Singleton<GameManager>, IObservable, IObserver
    {
        public IPlayer PlayerHuman;
        public IPlayer PlayerAi;
        public IPlayer CurrentPlayer;

        public void Init(GroupBox groupBox1ShipsUiHuman, GroupBox groupBox1ShipsUiAi, GroupBox OceanHumanPlayer, GroupBox OceanAiPlayer)
        {
            FireHumanCommand fireHumanCommand = new FireHumanCommand();
            PlayerHuman = new Player("Human", Color.Green, Color.CornflowerBlue, fireHumanCommand) { Ocean = new Ocean() };
            IPlayer playerHuman =PlayerHuman;
            FireAiCommand fireAiCommand = new FireAiCommand();
            PlayerAi = new Player("Atari 65XE", Color.Green, Color.CornflowerBlue, fireAiCommand) { Ocean = new Ocean() };
            IPlayer playerAi = PlayerAi;

            BoardView boardPlayer = new BoardView(playerHuman, OceanHumanPlayer);
            HudView hudPlayer = new HudView(playerHuman, groupBox1ShipsUiHuman);
            CreateObservers(playerHuman, boardPlayer, hudPlayer);
            playerHuman.CreateShips();
            playerHuman.GenerateOcean();
            playerHuman.PlaceShips(playerHuman.Ships);
            playerHuman.CanShoot = true;

            boardPlayer = new BoardView(playerAi, OceanAiPlayer);
            hudPlayer = new HudView(playerAi, groupBox1ShipsUiAi);
            CreateObservers(playerAi, boardPlayer, hudPlayer);
            playerAi.CreateShips();
            playerAi.GenerateOcean();
            playerAi.PlaceShips(playerAi.Ships);
            SetCurrentPlayer(playerHuman);
        }

        private void CreateObservers(IPlayer player, BoardView board, HudView hud)
        {
            player.AddObserver(board);
            player.AddObserver(hud);
        }

        public void ChangeCurrentPlayer()
        {
            CurrentPlayer.Victory= PlayerVictoryCheck(CurrentPlayer, GetOtherPlayer(CurrentPlayer));
            if (CurrentPlayer.Victory)
            {
                CurrentPlayer.CanShoot = false;
                NotifyObservers();
                return;
            }
            if (CurrentPlayer == PlayerHuman && PlayerAi!=null)
            {
                CurrentPlayer = PlayerAi;
                PlayerHuman.CanShoot = false;
                CurrentPlayer.FireCommand.Execute();
            }
            else if (CurrentPlayer == PlayerAi && PlayerHuman!= null)
            {
                CurrentPlayer = PlayerHuman;
                PlayerHuman.CanShoot = true;
            }
            NotifyObservers();
        }
        public void SetCurrentPlayer(IPlayer player)
        {
            CurrentPlayer = player;
            CurrentPlayer.CanShoot = true;
            NotifyObservers();
        }

        bool PlayerVictoryCheck(IPlayer current, IPlayer other)
        {
            bool allDestroyed=true;
            foreach (Ship ship in other.Ships)
            {
                if (!ship.Destroyed)
                {
                    allDestroyed = false;
                    return allDestroyed;
                }
            }
            return allDestroyed;
        }

        IPlayer GetOtherPlayer(IPlayer current)
        {
            if (current == GameManager.Instance.PlayerAi)
            {
                return GameManager.Instance.PlayerHuman;
            }
            else
            {
                return GameManager.Instance.PlayerAi;
            }
        }

        #region IObservable
        private List<IObserver> observers = new List<IObserver>();
        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.DataUpdate();
            }
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }
        #endregion

        public void DataUpdate()
        {
            throw new NotImplementedException();
        }

    }
}
