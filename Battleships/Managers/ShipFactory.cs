﻿using Battleships.Base;
using Battleships.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Managers
{
    public class ShipFactory: IShipFactory
    {
        public IShip CreateShip(Enums.ShipType type)
        {
            switch (type)
            {
                case Enums.ShipType.BattleCruiser:
                    return new BattleCruiser();
                case Enums.ShipType.Boat:
                    return new Boat();
                default:
                    return new Boat();
            }
        }
    }
}
