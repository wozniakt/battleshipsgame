﻿using Battleships.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Model
{
    public class ShipPart:IObservable
    {
        public Coordinates ShipPartCoordinates;
        private List<IObserver> observers = new List<IObserver>();

        private bool destroyed;
        public bool Destroyed
        {
            get
            {
                return destroyed;
            }
            set
            {
                destroyed = value;
                if (value==true)
                {
                    NotifyObservers();
                }
            }
        }

        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.DataUpdate();
            }
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }
    }
}
