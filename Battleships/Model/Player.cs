﻿using Battleships.Base;
using Battleships.Command;
using Battleships.Managers;
using Battleships.View;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.Model
{
    public class Player : IObservable, IPlayer
    {
        public string Name { get; set; }
        public bool CanShoot { get; set; }
        public bool Victory { get; set; }
        public Color OceanColor { get; set; }
        public Color ShipsColor { get; set; }
        public Ocean Ocean { get; set; }
        public OceanField CurrentOceanField { get; set; }
        public IFireCommand FireCommand { get; set; }
        public List<IShip> Ships { get; set; }
        private List<IObserver> observers = new List<IObserver>();

        public Player() { }

        public Player(string name, Color shipsColor, Color oceanColor, IFireCommand fireCommand)
        {
            Ships = new List<IShip>();
            FireCommand = fireCommand.Init(this);
            Name = name;
            ShipsColor = shipsColor;
            OceanColor = oceanColor;
        }

        public void CreateShips() 
        {
            IShipFactory shipFactory = new ShipFactory();
            IShip boat1 = shipFactory.CreateShip(Enums.ShipType.Boat);
            
            Ships.Add(boat1);
            IShip boat2 = shipFactory.CreateShip(Enums.ShipType.Boat);

            Ships.Add(boat2);
            IShip battleCruiser = shipFactory.CreateShip(Enums.ShipType.BattleCruiser);
 
            Ships.Add(battleCruiser);
        }

        public void GenerateOcean(int width = 10, int height = 10)
        {
            this.Ocean = new Ocean();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    OceanField oceanField = new OceanField();
                    CurrentOceanField = oceanField;
                    Ocean.Coordinates.Add(new Coordinates(i,j));
                    oceanField.Coordinates = new Coordinates(i, j);
                    this.NotifyObservers();
                    this.Ocean.OceanFields.Add(oceanField);
                }
            }
   
        }

       public void PlaceShips(List<IShip> ships) 
        {
            foreach (IShip ship in ships)
            {
                PlaceShipCommand placeShipCommand = new PlaceShipCommand(this,ship);
                placeShipCommand.Execute();
            }
        }



        #region IObservable
        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.DataUpdate();
            }
        }
        #endregion
    }
}
