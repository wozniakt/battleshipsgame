﻿using Battleships.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Model
{
    class BattleCruiser:Ship
    {
        public BattleCruiser()
        {
            Destroyed = false;
            this.ShipParts = new List<ShipPart>();
            for (int i = 0; i < 5; i++)
            {
                ShipPart shipPart = new ShipPart();
                shipPart.AddObserver(this);
                ShipParts.Add(shipPart);
            }
            NotifyObservers();
        }
    }
}
