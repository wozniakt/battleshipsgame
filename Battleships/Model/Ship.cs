﻿using Battleships.Base;
using Battleships.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.Model
{
    public class Ship: IObserver, IObservable, IShip
    {

        public string Name { get; set; }
        public bool Horizontal { get; set; }
        public bool Destroyed { get; set; }
        public List<ShipPart> ShipParts { get; set; }
        private List<IObserver> observers = new List<IObserver>();
        public  Ship()
        {
            Name = Randomizer.RandomName();
            RandomizeDirection();
        }

        private void RandomizeDirection()
        {
            Horizontal = true;
            var n = Randomizer.RandomNumber(0, 2);
            if (n==0)
            {
                Horizontal = false;
            } 
        }

        private bool IsDestroyed()
        {
            bool allPartsDestroyed = true;
            foreach (ShipPart shipPart in this.ShipParts)
            {
                if (shipPart.Destroyed==false)
                {
                    allPartsDestroyed = false;
      
                    return allPartsDestroyed;
                }
            }

            return allPartsDestroyed;
        }

        public void SetShipPartsCoordinates(Coordinates initCoords)
        {
            for (int i = 0; i < ShipParts.Count; i++)
            {
                ShipParts[i].ShipPartCoordinates = new Coordinates(initCoords.X, initCoords.Y + i);
            }
        }

        #region IObservator
        public void DataUpdate()
        {
            if (IsDestroyed())
            {
                Destroyed = true;
                NotifyObservers();
            }
        }
        #endregion

        #region IObservable

        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.DataUpdate();
            }
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }
        #endregion

        
    }
}
