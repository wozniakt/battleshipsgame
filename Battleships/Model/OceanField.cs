﻿using Battleships.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Model
{
   public class OceanField:IObservable
    {

        public bool AlreadyHit = false;
        public Coordinates Coordinates { get; set; }
        private List<IObserver> observers = new List<IObserver>();

        private ShipPart assignedShipPart;
        public ShipPart AssignedShipPart 
        {
            get
            {
                return assignedShipPart;
            }
            set
            {
                assignedShipPart = value;
                NotifyObservers();
            }
        }



        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.DataUpdate();
            }
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }
    }
}
