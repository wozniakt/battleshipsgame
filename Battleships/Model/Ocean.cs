﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Model
{
    public class Ocean
    {
        public List<OceanField> OceanFields = new List<OceanField>();
        public List<Coordinates> Coordinates = new List<Coordinates>();
        public int Width=10, Height=10;
    }
}
