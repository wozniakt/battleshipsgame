﻿using Battleships.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Model
{
   public class Boat:Ship
    {
        public Boat(): base()
        {
            Destroyed = false;
            this.ShipParts = new List<ShipPart>();
            for (int i = 0; i < 4; i++)
            {
                ShipPart shipPart = new ShipPart();
                shipPart.AddObserver(this);
                ShipParts.Add(shipPart);
            }
            NotifyObservers();
        }
    }
}
