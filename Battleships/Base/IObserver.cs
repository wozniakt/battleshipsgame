﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Base
{
    public interface IObserver
    {
        void DataUpdate();
    }
}
