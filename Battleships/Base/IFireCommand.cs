﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleships.Command;
using Battleships.Model;

namespace Battleships.Base
{
    public interface IFireCommand :ICommand
    {
        IFireCommand Init(IPlayer player);
        IPlayer DetermineTargetPlayer(IPlayer shootingPlayer);

    }
}
