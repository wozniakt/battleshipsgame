﻿using Battleships.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Managers
{
    public interface IShipFactory
    {
        IShip CreateShip(Enums.ShipType type);
    }
}
