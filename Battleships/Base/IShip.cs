﻿using Battleships.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Base
{
    public interface IShip:IObservable, IObserver
    {
        string Name { get; set; }
        bool Destroyed { get; set; }
        bool Horizontal { get; set; }

        List<ShipPart> ShipParts { get; set; }
        void SetShipPartsCoordinates(Coordinates initCoords);
    }
}
