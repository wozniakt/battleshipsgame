﻿using Battleships.Base;
using Battleships.Command;
using System.Collections.Generic;
using System.Drawing;

namespace Battleships.Model
{
    public interface IPlayer : IObservable
    {
        void GenerateOcean(int width = 10, int height = 10);
        void CreateShips();
        void PlaceShips(List<IShip> ships);
        OceanField CurrentOceanField { get; set; }
        IFireCommand FireCommand { get; set; }
        List<IShip> Ships { get; set; }
        Ocean Ocean { get; set; }
        string Name { get; set; }
        Color OceanColor { get; set; }
        Color ShipsColor { get; set; }
        bool CanShoot { get; set; }
        bool Victory { get; set; }
    }
}