﻿using Battleships.Base;
using Battleships.Command;
using Battleships.Managers;
using Battleships.Model;

using Battleships.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public partial class Form1 : Form, IObserver
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GameManager.Instance.AddObserver(this);
            GameManager.Instance.Init(ShipsUiHuman,ShipsUiAi, OceanHumanPlayer, OceanAiPlayer);

        }


        public void DataUpdate()
        {
            if (GameManager.Instance.CurrentPlayer.Victory)
            {
                labelCurrentPlayer.Text = GameManager.Instance.CurrentPlayer.Name + " Victory!!!";

            }
            else
            {
                labelCurrentPlayer.Text = GameManager.Instance.CurrentPlayer.Name + " is preparing to shoot";
            }

        }

    }
}
