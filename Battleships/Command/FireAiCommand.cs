﻿ using Battleships.Base;
using Battleships.Helpers;
using Battleships.Managers;
using Battleships.Model;
using Battleships.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.Command
{
    public class FireAiCommand : ICommand, IFireCommand
    {
        IPlayer shootingPlayer, targetPlayer;
        bool shootSucces=true;
        bool shootPossible=true;
        List<Coordinates> possibleCoordinates = new List<Coordinates>();

        public IPlayer DetermineTargetPlayer(IPlayer shootingPlayer)
        {
            if (shootingPlayer==GameManager.Instance.PlayerAi)
            {
                return GameManager.Instance.PlayerHuman;
            }
            else
            {
                return GameManager.Instance.PlayerAi;
            }
        }

        public void Execute()
        {
            if (shootPossible)
            {
                FireWithDelay(1000);
            }
            else
            {
                GameManager.Instance.ChangeCurrentPlayer();
            }

            if (shootSucces == false)
            {
                shootSucces = true;
                Execute();
            }
        }
        async void FireWithDelay(int time=1)
        {
            await Task.Delay(time);
            Fire();
        }

        public IFireCommand Init(IPlayer current)
        {
            shootingPlayer = current;
            return this;
        }

        private void Fire()
        {
            shootSucces = true;
            targetPlayer = DetermineTargetPlayer(shootingPlayer);
            if (possibleCoordinates.Count==0)
            {
                possibleCoordinates = new List<Coordinates>(targetPlayer.Ocean.Coordinates);
            }

            Coordinates coordinates= RandomCooridnates();
            OceanField oceanFieldResult = targetPlayer.Ocean.OceanFields.Find(a => a.Coordinates.X == coordinates.X && a.Coordinates.Y == coordinates.Y);
            targetPlayer.CurrentOceanField = oceanFieldResult;
            if (targetPlayer.CurrentOceanField.AlreadyHit == true)
            {
                shootSucces = false;
                //MessageBox.Show("Ai missed");
               return;
            }
            if (targetPlayer.CurrentOceanField.AssignedShipPart != null)
            {
                targetPlayer.CurrentOceanField.AssignedShipPart.Destroyed = true;
                targetPlayer.CurrentOceanField.NotifyObservers();
                targetPlayer.NotifyObservers();
            }
            else
            {
                targetPlayer.CurrentOceanField.AlreadyHit = true;
                targetPlayer.CurrentOceanField.NotifyObservers();
                targetPlayer.NotifyObservers();
            }
            if (possibleCoordinates.Count == 0)
            {
                shootPossible = false;
            }
                GameManager.Instance.ChangeCurrentPlayer();
        }
        Coordinates RandomCooridnates()
        {
            var rnd = Randomizer.RandomNumber(0, possibleCoordinates.Count);
            Coordinates coordinates = possibleCoordinates[rnd];
            if (ValidCoordinates(coordinates) == false)
            {
                coordinates = RandomCooridnates();
                
            }
            possibleCoordinates.Remove(coordinates);
            return coordinates;
        }


        bool ValidCoordinates(Coordinates coordinates)
        {
            bool validCoords = true;
                
                OceanField oceanFieldResult =targetPlayer.Ocean.OceanFields.Find(a => a.Coordinates.X == coordinates.X && a.Coordinates.Y == coordinates.Y);

                if (oceanFieldResult.AlreadyHit == true)
                {
                    validCoords = false;
                }
                if (oceanFieldResult.AssignedShipPart != null && oceanFieldResult.AssignedShipPart.Destroyed==true)
                {
                    validCoords = false;
                }
            return validCoords;
        }
    }
}
