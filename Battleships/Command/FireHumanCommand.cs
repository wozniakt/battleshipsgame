﻿using Battleships.Base;
using Battleships.Managers;
using Battleships.Model;
using Battleships.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.Command
{
    public class FireHumanCommand : ICommand, IFireCommand
    {
        IPlayer shootingPlayer;
        IPlayer targetPlayer;
        bool shootSucces;
        public void Execute()
        {
            if (shootingPlayer.CanShoot)
            {
                Fire();

            }
        }

        public IPlayer DetermineTargetPlayer(IPlayer shootingPlayer)
        {
            if (shootingPlayer == GameManager.Instance.PlayerAi)
            {
                return GameManager.Instance.PlayerHuman;
            }
            else
            {
                return GameManager.Instance.PlayerAi;
            }
        }

        public IFireCommand Init(IPlayer current)
       {
            shootingPlayer = current;
    
            return this;
       }

        private void Fire()
        {
            shootSucces = true;
            targetPlayer = DetermineTargetPlayer(shootingPlayer);
            targetPlayer.CurrentOceanField = shootingPlayer.CurrentOceanField;
            if (targetPlayer.CurrentOceanField.AlreadyHit==true || (targetPlayer.CurrentOceanField.AssignedShipPart!=null && targetPlayer.CurrentOceanField.AssignedShipPart.Destroyed) )
            {
                shootSucces = false;
                MessageBox.Show("This one was already hit. You miss your turn");
                GameManager.Instance.ChangeCurrentPlayer();
                return;
            }
            if (shootingPlayer.CurrentOceanField.AssignedShipPart != null)
            {
                targetPlayer.CurrentOceanField.AssignedShipPart.Destroyed = true;
                shootingPlayer.CurrentOceanField.NotifyObservers();
                targetPlayer.NotifyObservers();
            }
            else
            {
                targetPlayer.CurrentOceanField.AlreadyHit = true;
                shootingPlayer.CurrentOceanField.NotifyObservers();
                targetPlayer.NotifyObservers();
            }
            GameManager.Instance.ChangeCurrentPlayer();
        }
    }
}
