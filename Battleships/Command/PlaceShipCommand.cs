﻿using Battleships.Base;
using Battleships.Helpers;
using Battleships.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships.Command
{
    public class PlaceShipCommand : ICommand
    {
        Player currentPlayer;
        IShip currentShip;

        List<Coordinates> possibleCoordinates ;
        List<OceanField> oceanFields;
        OceanField oceanFieldResult;
        Coordinates coords;
        public void Execute()
        {
            oceanFields=new List<OceanField> (currentPlayer.Ocean.OceanFields);
            Coordinates coords= RandomCooridnates();
            PlaceShip(coords);
        }

        public PlaceShipCommand(Player player, IShip ship)
        {
            currentPlayer = player;
            currentShip = ship;
      
        }

        Coordinates RandomCooridnates()
        {

            Coordinates coordinates;
            if (possibleCoordinates == null)
            {
                possibleCoordinates = new List<Coordinates>(currentPlayer.Ocean.Coordinates);
            }
            var rnd = Randomizer.RandomNumber(0, possibleCoordinates.Count);
            try
            {
                 coordinates = possibleCoordinates[rnd];
            }
            catch (ArgumentOutOfRangeException)
            {
                //Too many ships;
                throw;
            }

            if (ValidCoordinates(coordinates) ==false || ValidCoordinatesBorders(coordinates)==false)
            {
                possibleCoordinates.Remove(coordinates);
                coordinates = RandomCooridnates();
            }
            possibleCoordinates.Remove(coordinates);
            return coordinates;
        }

        bool ValidCoordinates(Coordinates coordinates)
        {
            bool validCoords=true;
            for (int i = 0; i < currentShip.ShipParts.Count; i++)
            {
                 
                if (currentShip.Horizontal)
                {
                     coords = new Coordinates(coordinates.X + i, coordinates.Y);
                }
                else
                {
                     coords = new Coordinates(coordinates.X , coordinates.Y+i);
                }

                oceanFieldResult  = oceanFields.Find(a => a.Coordinates.X == coords.X && a.Coordinates.Y == coords.Y);
                //GC.Collect();
                if (oceanFieldResult==null)
                {
                    {
                        validCoords = false;
                        break;
                    }
                }
                if (oceanFieldResult.AssignedShipPart!=null)
                {
                    validCoords = false;
                    break;
                }
            }
            return validCoords;
        }

        bool ValidCoordinatesBorders(Coordinates coordinates)
        {
            if (currentShip.Horizontal)
            {
                int n = (currentPlayer.Ocean.Width - coordinates.X);
                return ((currentPlayer.Ocean.Width - coordinates.X) > currentShip.ShipParts.Count);
            }
            else
            {
                int n = (currentPlayer.Ocean.Width - coordinates.Y);
                return ((currentPlayer.Ocean.Height - coordinates.Y) > currentShip.ShipParts.Count);
            }
        }

        private void PlaceShip(Coordinates coordinates)
        {
            for (int i = 0; i < currentShip.ShipParts.Count; i++)
            {
                Coordinates coords;
                if (currentShip.Horizontal)
                {
                    coords = new Coordinates(coordinates.X + i, coordinates.Y);
                }
                else
                {
                    coords = new Coordinates(coordinates.X, coordinates.Y + i);
                }
                OceanField oceanFieldResult = oceanFields.Find(a => a.Coordinates.X == coords.X && a.Coordinates.Y == coords.Y);
                
                currentPlayer.CurrentOceanField = oceanFieldResult;
                oceanFieldResult.AssignedShipPart = currentShip.ShipParts[i];
                currentPlayer.CurrentOceanField.NotifyObservers();
            }
            currentShip.SetShipPartsCoordinates(coordinates);
            currentShip.NotifyObservers();
        }


    }
}
