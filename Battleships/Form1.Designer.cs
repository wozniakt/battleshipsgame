﻿namespace Battleships
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ShipsUiHuman = new System.Windows.Forms.GroupBox();
            this.OceanHumanPlayer = new System.Windows.Forms.GroupBox();
            this.ShipsUiAi = new System.Windows.Forms.GroupBox();
            this.OceanAiPlayer = new System.Windows.Forms.GroupBox();
            this.labelCurrentPlayer = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ShipsUiHuman
            // 
            this.ShipsUiHuman.Location = new System.Drawing.Point(12, 33);
            this.ShipsUiHuman.Name = "ShipsUiHuman";
            this.ShipsUiHuman.Size = new System.Drawing.Size(271, 282);
            this.ShipsUiHuman.TabIndex = 4;
            this.ShipsUiHuman.TabStop = false;
            this.ShipsUiHuman.Text = "Human Ships";
            // 
            // OceanHumanPlayer
            // 
            this.OceanHumanPlayer.Location = new System.Drawing.Point(289, 33);
            this.OceanHumanPlayer.Name = "OceanHumanPlayer";
            this.OceanHumanPlayer.Size = new System.Drawing.Size(295, 282);
            this.OceanHumanPlayer.TabIndex = 3;
            this.OceanHumanPlayer.TabStop = false;
            this.OceanHumanPlayer.Text = "Human  Ocean";
            // 
            // ShipsUiAi
            // 
            this.ShipsUiAi.Location = new System.Drawing.Point(889, 33);
            this.ShipsUiAi.Name = "ShipsUiAi";
            this.ShipsUiAi.Size = new System.Drawing.Size(264, 282);
            this.ShipsUiAi.TabIndex = 6;
            this.ShipsUiAi.TabStop = false;
            this.ShipsUiAi.Text = "Computer Ships";
            // 
            // OceanAiPlayer
            // 
            this.OceanAiPlayer.Location = new System.Drawing.Point(590, 33);
            this.OceanAiPlayer.Name = "OceanAiPlayer";
            this.OceanAiPlayer.Size = new System.Drawing.Size(293, 282);
            this.OceanAiPlayer.TabIndex = 5;
            this.OceanAiPlayer.TabStop = false;
            this.OceanAiPlayer.Text = "Computer Ocean";
            // 
            // labelCurrentPlayer
            // 
            this.labelCurrentPlayer.AutoSize = true;
            this.labelCurrentPlayer.Location = new System.Drawing.Point(451, 346);
            this.labelCurrentPlayer.Name = "labelCurrentPlayer";
            this.labelCurrentPlayer.Size = new System.Drawing.Size(0, 13);
            this.labelCurrentPlayer.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1165, 395);
            this.Controls.Add(this.labelCurrentPlayer);
            this.Controls.Add(this.ShipsUiAi);
            this.Controls.Add(this.ShipsUiHuman);
            this.Controls.Add(this.OceanAiPlayer);
            this.Controls.Add(this.OceanHumanPlayer);
            this.Name = "Form1";
            this.Text = "Battleships!";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox ShipsUiHuman;
        private System.Windows.Forms.GroupBox OceanHumanPlayer;
        private System.Windows.Forms.GroupBox ShipsUiAi;
        private System.Windows.Forms.GroupBox OceanAiPlayer;
        private System.Windows.Forms.Label labelCurrentPlayer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
    }
}

