﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Helpers
{
    public static class Randomizer
    {
        private static readonly Random random = new Random();
        static List<string> firstNames = new List<string>() { "Black", "Blue", "Dark", "Angry", "Bloody", "Hungry", "Rusty", "White" };
        static List<string> secondNames = new List<string>() { "Sword", "Knight", "Angel", "Siren", "Boat", "Fish", "Eye", "Shark" };

        public static int RandomNumber(int min, int max)
        {
            return random.Next(min, max);
        }

        public static string RandomName()
        {
            var rnd1 = Randomizer.RandomNumber(0, firstNames.Count);
            var rnd2 = Randomizer.RandomNumber(0, secondNames.Count);
            string firstName = firstNames[rnd1];
            string secondName = secondNames[rnd2];
            firstNames.RemoveAt(rnd1);
            secondNames.RemoveAt(rnd2);
            if (firstNames.Count<=0)
            {
                firstNames.Add("Simple");
            }
            if (secondNames.Count <= 0)
            {
                secondNames.Add("Boat");
            }
            return String.Format("{0} {1}", firstName, secondName);
        }
    }
}