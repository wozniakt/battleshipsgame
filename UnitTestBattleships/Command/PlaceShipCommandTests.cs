﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships.Command;
using System;
using System.Collections.Generic;
using NUnit.Framework;
using Battleships.Base;
using Battleships.Model;
using System.Drawing;

namespace Battleships.Managers.Tests
{
    [TestClass()]
    public class PlaceShipCommandTests
    {
        [Test()]
        public void PlaceShipCommandExecuteTestStandardConfig()
        {
            FireAiCommand fireAiCommand = new FireAiCommand();
            Player PlayerDummy = new Player("DummyAIPlayer", Color.Green, Color.CornflowerBlue, fireAiCommand) { Ocean = new Ocean() };
            NUnit.Framework.Assert.IsNotNull(PlayerDummy);
            PlayerDummy.CreateShips();
            NUnit.Framework.Assert.IsNotNull(PlayerDummy.Ships);
            PlayerDummy.GenerateOcean();
            NUnit.Framework.Assert.IsNotNull(PlayerDummy.Ocean.OceanFields);
            PlayerDummy.PlaceShips(PlayerDummy.Ships);
            NUnit.Framework.Assert.Pass("Ships placed succesfully");
        }

        [TestCase(500)]
        [TestCase(200)]
        [TestCase(100)]
        public void PlaceShipCommandExecuteTestLotOfShips_ExceptionHandled(int shipsCount)
        {
            FireAiCommand fireAiCommand = new FireAiCommand();
            Player PlayerDummy = new Player("DummyAiPlayer", Color.Green, Color.CornflowerBlue, fireAiCommand) { Ocean = new Ocean() };
            NUnit.Framework.Assert.IsNotNull(PlayerDummy);

            PlayerDummy.Ships=new List<IShip>(CreateDummyShips(shipsCount));
            NUnit.Framework.Assert.IsNotNull(PlayerDummy.Ships);
            PlayerDummy.GenerateOcean();
            NUnit.Framework.Assert.IsNotNull(PlayerDummy.Ocean.OceanFields);
            
            NUnit.Framework.Assert.Throws<ArgumentOutOfRangeException>(()=> PlayerDummy.PlaceShips(PlayerDummy.Ships));
        }

        [TestCase(10)]
        public void PlaceShipCommandExecuteTest10Ships_NoException(int shipsCount)
        {
            FireAiCommand fireAiCommand = new FireAiCommand();
            Player PlayerDummy = new Player("DummyAiPlayer", Color.Green, Color.CornflowerBlue, fireAiCommand) { Ocean = new Ocean() };
            NUnit.Framework.Assert.IsNotNull(PlayerDummy);

            PlayerDummy.Ships = new List<IShip>(CreateDummyShips(shipsCount));
            NUnit.Framework.Assert.IsNotNull(PlayerDummy.Ships);
            PlayerDummy.GenerateOcean();
            NUnit.Framework.Assert.IsNotNull(PlayerDummy.Ocean.OceanFields);

            NUnit.Framework.Assert.DoesNotThrow(() => PlayerDummy.PlaceShips(PlayerDummy.Ships));
        }

        List<IShip> CreateDummyShips(int count)
        {
            List<IShip> ships=new List<IShip>();
            for (int k = 0; k < count; k++)
            {
                IShipFactory shipFactory = new ShipFactory();
                IShip ship = shipFactory.CreateShip(Enums.ShipType.Boat);
                NUnit.Framework.Assert.IsNotNull(ship);
                ships.Add(ship);
            }
            return ships;
        }

    }
}