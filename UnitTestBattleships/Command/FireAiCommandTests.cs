﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Drawing;
using Battleships.Model;
using Battleships.Managers;

namespace Battleships.Command.Tests
{
    [TestClass()]
    public class FireAiCommandTests
    {
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(100)]
        [TestCase(500)]
        public void FireAiCommandExecuteTestMultipleShots(int fireCounts)
        {
            IPlayer playerDummyHuman = PrepareDummyPlayerHuman();
            IPlayer playerDummyAi = PrepareDummyPlayerAi();
            GameManager.Instance.PlayerAi = playerDummyAi;
            GameManager.Instance.PlayerHuman = playerDummyHuman;
            GameManager.Instance.SetCurrentPlayer(playerDummyAi);

            NUnit.Framework.Assert.DoesNotThrow(()=>
            {
                for (int i = 0; i < fireCounts; i++)
                {
                    playerDummyAi.FireCommand.Execute();
                }
            });
        }

        IPlayer PrepareDummyPlayerHuman()
        {
            FireHumanCommand fireHumanCommand = new FireHumanCommand();
            Player playerDummyHuman = new Player();
            fireHumanCommand.Init(playerDummyHuman);
            playerDummyHuman = new Player("DummyHumanPlayer", Color.Green, Color.CornflowerBlue, fireHumanCommand) { Ocean = new Ocean() };
            NUnit.Framework.Assert.IsNotNull(playerDummyHuman);
            playerDummyHuman.CreateShips();
            NUnit.Framework.Assert.IsNotNull(playerDummyHuman.Ships);
            playerDummyHuman.GenerateOcean();
            NUnit.Framework.Assert.IsNotNull(playerDummyHuman.Ocean.OceanFields);
            playerDummyHuman.PlaceShips(playerDummyHuman.Ships);
            return playerDummyHuman;
        }

        IPlayer PrepareDummyPlayerAi()
        {
            FireAiCommand fireAiCommand = new FireAiCommand();
            Player playerDummyAi = new Player(); ;
            fireAiCommand.Init(playerDummyAi);
            playerDummyAi = new Player("DummyAiPlayer", Color.Green, Color.CornflowerBlue, fireAiCommand) { Ocean = new Ocean() };
            NUnit.Framework.Assert.IsNotNull(playerDummyAi);
            playerDummyAi.CreateShips();
            NUnit.Framework.Assert.IsNotNull(playerDummyAi.Ships);
            playerDummyAi.GenerateOcean();
            NUnit.Framework.Assert.IsNotNull(playerDummyAi.Ocean.OceanFields);
            playerDummyAi.PlaceShips(playerDummyAi.Ships);
            return playerDummyAi;
        }

        OceanField DummyTargetOceanField(int x, int y, bool alreadyHit)
        {
            OceanField oceanField = new OceanField();
            oceanField.Coordinates = new Coordinates(x,y);
            oceanField.AlreadyHit = false;
            oceanField.AssignedShipPart = new ShipPart() { Destroyed=false, ShipPartCoordinates=new Coordinates(x,y)};
            return oceanField;
        }

    }
}