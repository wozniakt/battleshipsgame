﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleships.Base;
using NUnit.Framework;

namespace Battleships.Managers.Tests
{
    [TestClass()]
    public class ShipFactoryTests
    {
        [TestCase(100, Enums.ShipType.Boat)]
        [TestCase(100, Enums.ShipType.BattleCruiser)]
        public void CreateShipsTestNotNull([Random(int.MinValue, int.MaxValue, 100)] int count,
            Enums.ShipType boatType)
        {
            for (int k = 0; k < count; k++)
            {
                IShipFactory shipFactory = new ShipFactory();
                IShip boat = shipFactory.CreateShip(boatType);
                NUnit.Framework.Assert.IsNotNull(boat);
            }

        }
    }
}